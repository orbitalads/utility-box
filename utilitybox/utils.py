

def _finditems(obj, key):
    """
    find all items recursively in a dict whose key is key parameter

    :param obj: dict to look into
    :param key: key to look for recursively in obj dict
    :return: all values in obj and inner dicts whose key is param key
    """
    if key in obj:
        yield obj[key]
    if isinstance(obj, dict):
        for k, v in obj.items():
            if isinstance(v, dict):
                for item in _finditems(v, key):
                    yield item


def group_by(iterable, *keys):
    """
    Group iterable by key provided in a dict

    :param iterable: iterable data structure to group by key
    :param *keys: functions applied to each item from iterable to create the groups for the items.
    Restulting key from functions must be hashable
    :return list of tuples, representing each group
    """
    group_keys = []
    grouped_items = []

    if not keys:
        keys = [lambda x: x]

    for item in iterable:
        keys_set = {key(item) for key in keys}
        try:
            idx = group_keys.index(keys_set)
        except ValueError: #keys_set does not exists, create new group
            group_keys.append(keys_set)
            grouped_items.append([item])
        else: # keys_set already existed, append item to corresponding group
            grouped_items[idx].append(item)

    return zip(group_keys, grouped_items)