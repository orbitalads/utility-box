from functools import wraps
from utilitybox.exception import HTTPException, Error400,\
    Error401, Error403, Error404, Error202


class VoidController(object):
    def __init__(self, base_route):
        self.base_route = base_route

    def return_handler(self, f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except HTTPException as exc:
                if isinstance(exc, Error400):
                    return "Bad request", 400
                elif isinstance(exc, Error401):
                    return "Authentication required", 401
                elif isinstance(exc, Error403):
                    return "Forbidden", 403
                elif isinstance(exc, Error404):
                    return "Not Found", 404
                elif isinstance(exc, Error202):
                    return "Pending analysis", 202
                # Any other error not captured
                raise
        return decorated_function

    def attach_to(self, app):
        @app.route(self.base_route, methods=['GET'])
        @self.return_handler
        def ok_function(*args, **kwargs):
            return '', 204
