class Oauth2ServerException(Exception):
    pass


class HTTPException(Oauth2ServerException):
    pass


class Error404(HTTPException):  # Not FOUND
    pass


class Error403(HTTPException):  # Forbidden
    pass


class Error401(HTTPException):  # Unauthorized
    pass


class Error400(HTTPException):  # Bad request
    pass


class Error202(HTTPException):  # Not ready yet
    pass
