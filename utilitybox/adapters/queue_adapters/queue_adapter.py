from abc import ABCMeta, abstractmethod


class QueueAdapter(metaclass=ABCMeta):
    """
    An interface that defines the behaviour of an adapter for a Queue service that
    follows Publisher-Subscriber Pattern.
    """

    # PUBLISHER
    @abstractmethod
    def create_topic(self, topic_name):
        pass

    @abstractmethod
    def delete_topic(self, topic):
        pass

    @abstractmethod
    def create_message(self, data):
        pass

    @abstractmethod
    def send(self, topic, message):
        pass

    # SUBSCRIBER
    @abstractmethod
    def subscribe(self, topic):
        pass

    @abstractmethod
    def unsubscribe(self, subscription):
        pass

    @abstractmethod
    def receive(self, subscription):
        pass

    # GENERAL
    @abstractmethod
    def list_topics(self):
        pass

    @abstractmethod
    def list_subscriptions(self):
        pass

    @abstractmethod
    def topic_exists(self, topic_name):
        pass

    @abstractmethod
    def subscription_exists(self, subscription):
        pass


class Topic(metaclass=ABCMeta):
    """
    An interface that defines the behaviour of a Topic in the Publisher/Subscriber pattern
    """

    def __init__(self, topic_name, queue):
        self.name = topic_name
        self.queue = queue

    def get_name(self):
        return self.name

    def get_queue(self):
        return self.queue

    @abstractmethod
    def exists(self):
        pass

    @abstractmethod
    def send(self, message):
        pass


class Subscription(metaclass=ABCMeta):
    """
    An interface that defines the behaviour of a Subscription in the Publisher/Subscriber pattern
    """

    def __init__(self, subscription_name, topic, queue):
        self.name = subscription_name
        self.topic = topic
        self.queue = queue

    def get_subscription_name(self):
        return self.name

    def get_topic(self):
        return self.topic

    def get_queue(self):
        return self.queue

    @abstractmethod
    def exists(self):
        pass

    @abstractmethod
    def receive(self):
        pass


class Message(metaclass=ABCMeta):
    """
    An interface that defines the behaviour of a Message in the Publisher/Subscriber pattern
    """

    def __init__(self, message_id, data):
        self.id = message_id
        self.data = data

    def get_message_id(self):
        return self.id

    def set_message_id(self, message_id):
        self.id = message_id

    @abstractmethod
    def set_data(self, data):
        pass

    @abstractmethod
    def get_data(self):
        pass
