from utilitybox.adapters.queue_adapters.queue_adapter import QueueAdapter


# TODO
class InMemoryQueueAdapter(QueueAdapter):
    """
    An adapter for In Memory service that implements QueueAdapter
    """

    def __init__(self, project_id):
        pass

    # PUBLISHER
    def create_topic(self, topic_name):
        pass

    def delete_topic(self, topic):
        pass

    def send(self, topic=None, topic_name="", message=None):
        pass

    # SUBSCRIBER
    def subscribe_as_pull(self, topic_name):
        pass

    def subscribe_as_push(self, topic_name):
        pass

    def change_subscription_to_pull(self, topic_name):
        pass

    def change_subscription_to_push(self, topic_name):
        pass

    def receive(self, subscription=None, topic=None, topic_name="", **kwargs):
        pass

    # GENERAL
    def list_topics(self):
        pass

    def list_subscriptions(self):
        pass

    def topic_exists(self, topic_name):
        pass

    def subscription_exists(self, subscription=None):
        pass

    def unsubscribe(self, topic=None):
        pass
