from google.cloud import pubsub_v1
import time

from google.gax import errors, GaxError


# Topics

def create_topic(project, topic_name):
    """Create a new Pub/Sub topic. Best Effort. """
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project, topic_name)
    try:
        topic = publisher.create_topic(topic_path)
        print('Topic was created on the cloud: {}'.format(topic))
    except GaxError as e:
        if "ALREADY_EXISTS" not in str(e.cause):
            raise e
        print("Topic already exist.. but that's OK!")


def list_topics(project):
    """Lists all Pub/Sub topics in the given project."""
    publisher = pubsub_v1.PublisherClient()
    project_path = publisher.project_path(project)

    for topic in publisher.list_topics(project_path):
        print(topic)


def delete_topic(project, topic_name):
    """Deletes an existing Pub/Sub topic."""
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project, topic_name)
    publisher.delete_topic(topic_path)
    print('Topic deleted: {}'.format(topic_path))


# Subscriptions

def create_subscription(project, topic_name, subscription_name):
    """Create a new pull subscription on the given topic."""
    subscriber = pubsub_v1.SubscriberClient()
    topic_path = subscriber.topic_path(project, topic_name)
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    subscription = subscriber.create_subscription(
        subscription_path, topic_path)

    print('Subscription created: {}'.format(subscription))


def list_subscriptions(project, topic_name):
    """Lists all subscriptions for a given topic."""
    subscriber = pubsub_v1.SubscriberClient()
    topic_path = subscriber.topic_path(project, topic_name)

    for subscription in subscriber.list_subscriptions(topic_path):
        print(subscription.name)


def delete_subscription(project, subscription_name):
    """Deletes an existing Pub/Sub topic."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    subscriber.delete_subscription(subscription_path)

    print('Subscription deleted: {}'.format(subscription_path))


# Messaging

def publish_message(project, topic_name, data, attributes):
    """Publishes a messages to a Pub/Sub topic."""
    print("sending message")
    print("data={}".format(data))
    print("attributes={}".format(attributes))
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project, topic_name)
    # Data must be a byte-string
    data = str(data).encode('utf-8')
    parsed_attributes = {}
    for key, value in attributes.items():
        parsed_attributes[key] = str(value)
    publisher.publish(topic_path, data=data, **parsed_attributes)
    print('Message sent.')


def publish_messages_with_futures(project, topic_name):
    """Publishes multiple messages to a Pub/Sub topic and prints their
    message IDs."""
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project, topic_name)

    # When you publish a message, the client returns a Future. This Future
    # can be used to track when the message is published.
    futures = []

    for n in range(1, 10):
        data = u'Message number {}'.format(n)
        # Data must be a bytestring
        data = data.encode('utf-8')
        message_future = publisher.publish(topic_path, data=data)
        futures.append(message_future)

    print('Published message IDs:')
    for future in futures:
        # result() blocks until the message is published.
        print(future.result())


def publish_messages_with_batch_settings(project, topic_name):
    """Publishes multiple messages to a Pub/Sub topic with batch settings."""
    # Configure the batch to publish once there is one kilobyte of data or
    # 1 second has passed.
    batch_settings = pubsub_v1.types.BatchSettings(
        max_bytes=1024,  # One kilobyte
        max_latency=1,  # One second
    )
    publisher = pubsub_v1.PublisherClient(batch_settings)
    topic_path = publisher.topic_path(project, topic_name)

    for n in range(1, 10):
        data = u'Message number {}'.format(n)
        # Data must be a bytestring
        data = data.encode('utf-8')
        publisher.publish(topic_path, data=data)

    print('Published messages.')


def receive_messages(project, subscription_name):
    """Receives messages from a pull subscription."""
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(
        project, subscription_name)

    def callback(message):
        print('Received message: {}'.format(message))
        message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking, so we must keep the main thread from
    # exiting to allow it to process messages in the background.
    print('Listening for messages on {}'.format(subscription_path))
    while True:
        time.sleep(60)


