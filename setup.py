from setuptools import setup, find_packages


url = ""
# packages = ["utilitybox", "utilitybox/controllers", "utilitybox/adapters",
#             "utilitybox/adapters/queue_adapters",
#             "utilitybox/adapters/queue_adapters/google_pubsub",
#             "utilitybox/adapters/queue_adapters/in_memory"]

requirements = ['google-cloud-pubsub',]

setup_requirements = [
    # TODO(bertucho): put setup requirements (distutils extensions, etc.) here
]

test_requirements = [
    # TODO: put package test requirements here
]


setup(
    name="utilitybox",
    version='2.3.0',
    author="orbitalADS",
    author_email="dev@orbitalads.io",
    url=url,
    # packages=find_packages(include=['utilitybox', 'utilitybox.*']),
    packages=find_packages(include=['utilitybox']),
    include_package_data=True,
    install_requires=requirements,
    zip_safe=False,
    keywords='utilitybox',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements
)
# Use install_requires to indicate the packages that are needed to use the
# package

