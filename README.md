# OrbitalAds Utility Box

This repository is a collection of classes used in several of the OrbitalAds python based microservices. It's built as a PIP package that can be added as a requirement for any needed microservice.

Just add this line in your requirements.txt
```
-e git+https://bitbucket.org/orbitalads/utility-box.git#egg=utilitybox
```


### What utilities are contained in this package? ###

* Void Controller
* Queue Adapters
* Exception

## Void controller ##
It's a Python module with a class that can receive an endpoint and return HTTP 200/OK to all request received.

It's intended to work with Flask.
```
from utilitybox.controllers.void_controller import VoidController
VoidController("/").attachTo(self.app)
```

## Queue Adapter ##

It's a Python module that contains an abstract queue adapter providing a common interface, and the classes that extends the abstract queue adapter class

```
from utilitybox.adapters.queue_adapters.pubsub.pubsub_queue_adapter import PubSubQueueAdapter
```
### Now contains ###
- PubSubQueueAdapter
- InMemoryQueueAdapter

## Exception ##
It's a Python module with a few classes that doesn't do anything in this moment.

It's a common dependency of a few Flask based microservice.

# Adding utilities in the package #

1. Clone this repository in your computer
2. Change, or add modules in the utilitybox folder.
3. Update the setup.py, or setup.cfg if needed.
4. Update the readme.md file

Be sure to update the requirements as explained in setup.py if your changes needs another package not included.

## Known issues ##
